﻿using System;
using System.Collections.Generic;
using System.Threading;
using Task3.Helpers;
using Task3.Interfaces;
using Task3.Services;
using MongoDB.Driver;

namespace Task3
{
    class Program
    {
        static void Main(string[] args)
        {

            Bank myBank = new Bank("AccessBank");
            List<DepositAccount> bankaccts = CreateDummyAccounts();

            foreach (DepositAccount acct in bankaccts)
            {
                //Add new accounts to bank
                //myBank.AddNewAccount(acct, isCashier: false);
            }

            Console.WriteLine("\t\t##########################################");
            Console.WriteLine($"\n\t\tAccounts in bank:");
            myBank.Summary();
            myBank.AddToAccount("2516881858", 6500);
            myBank.AddToAccount("6651766536", 6500);
            myBank.WithdrawFromAccount("2516881858", 3000);
            myBank.WithdrawFromAccount("6651766536", 3000);
            myBank.TransferCash("2516881858", "6651766536", 1000);
            Console.WriteLine(myBank.GetBankTransactionHistory());

            //Console.ReadLine();

            Thread.Sleep(1000);
            myBank.ViewAccountHistory("0364878018");
            Thread.Sleep(1000);
            myBank.ViewAccountHistory("8466361745");
            Console.ReadLine();
            //Cashier.StartWork(myBank);
            object

            
           
        }

        static List<DepositAccount> CreateDummyAccounts()
        {

            PersonCustomer cust11 = new PersonCustomer("Mr", "Emma", "Okon", "04/04/1995", Gender.Female, "10 dhckbjkhk");
            PersonCustomer cust1 = new PersonCustomer("Prof Mrs", "Joy", "Okon", "04/04/1995", Gender.Male, "10 djkhk");
            PersonCustomer cust2 = new PersonCustomer("Engr ", "David", "Ubong", "04/04/1995", Gender.Female, "12 djkhk");
            DepositAccount new1 = new DepositAccount(cust1, DepositAccountType.Student, startBalance: 1000);
            Thread.Sleep(100);
            DepositAccount new2 = new DepositAccount(cust2, DepositAccountType.Current, startBalance: 105000);
            Thread.Sleep(100);
            DepositAccount new9 = new DepositAccount(cust11, DepositAccountType.Savings, startBalance: 10000);
            Thread.Sleep(100);
            List<DepositAccount> bankaccts = new List<DepositAccount>() { new1, new2, new9 };
            return bankaccts;
        }
    }


}



