﻿using System;
using System.Collections.Generic;
using System.Text;
using Task3.Services;
using Task3.Interfaces;

namespace Task3.Helpers
{
    class Utility
    {
        public static Tuple<bool, DepositAccount> FindAccount(List<DepositAccount> accounts,string AcctNumber)
        {
            DepositAccount acct;
            foreach (DepositAccount account in accounts)
            {
                if (account.AccountNumber == AcctNumber)
                {
                    acct = account;
                    return Tuple.Create(true, acct);
                }
            }
            return Tuple.Create(false, new DepositAccount());
        }
        public static DepositAccountType GetDepositAccountType(String accountType)
        {
            if (accountType.ToLower().Trim() == "savings")
            {
                return DepositAccountType.Savings;
            }
            else if (accountType.ToLower().Trim() == "student")
            {
                return DepositAccountType.Student;
            }
            else if (accountType.ToLower().Trim() == "current")
            {
                return DepositAccountType.Current;
            }
            else
            {
                throw new ArgumentException($"{accountType.ToUpper()} is not a valid Deposit account Type");
            }
        }


        internal static Decimal GetDecimal(string input)
        {
            try
            {
                return Convert.ToDecimal(input);
            }
            catch
            {
                throw new ArgumentException("Invalid Start Balance amount");
            }
        }

        internal static bool  CheckDOB(string dob)
        {
            Exception e = new Exception($"Invalid Date String Argument");
            try
            {
                int age = DateTime.Now.Year - Convert.ToDateTime(dob).Year;
                if (age < 0)
                {
                    Console.WriteLine($"Error: Date of Birth cannot be older than{DateTime.Now }");
                    return false;
                }
                    
                else
                    return true;
            }
            catch
            {
                Console.WriteLine($"Error: {dob } is not a valid date");
                return false;
                //throw e;
            }
        }

        internal static bool IsValidStartBalance(DepositAccountType accountType, decimal startBalance)
        {
            bool isValid = false;
            switch ((int)accountType)
            {
                case 0://students
                    if (startBalance < 0)
                        isValid = false;
                        
                    else
                        isValid = true;
                    break;
                case 1000:// savings
                    if (startBalance < 1000)
                        isValid = false;
                    else
                        isValid = true;
                    break;
                case 10000: //current
                    if (startBalance < 10000)
                        isValid = false;
                    else
                        isValid = true;
                    break;

            }
            return isValid;
        }

        public static Gender getGender(string gender)
        {
            if (gender.Trim().ToLower() == "male") { return Gender.Male; }
            else if (gender.Trim().ToLower() == "female") { return Gender.Female; }
            else if (gender.Trim().ToLower() == "non binary") { return Gender.NonBinary; }
            else { throw new Exception("Invalid Gender"); }
        }
    }
}
