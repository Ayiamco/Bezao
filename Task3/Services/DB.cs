﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace Task3.Services
{
    
    class DB<T>
    {
        public  delegate void DeleteItem(IMongoCollection<T> collection, string AccountNumber);
        static MongoClient client;
        static IMongoDatabase db;
        readonly IMongoCollection<T> collection;

        public DB(string Database, string TableName)
        {
            client = new MongoClient();
            db = client.GetDatabase(Database);
            collection = db.GetCollection<T>(TableName);
        }

        public void CreateOne(T student)
        {//function to add object to database
            collection.InsertOne(student);
        }
        public List<T> ReadAll()
        {
            List<T> list = collection.AsQueryable().ToList<T>();
            return list;
        }

        public T ReadOne(string searchId, string value)
        {
            var filter = Builders<T>.Filter.Eq(searchId,value);
            return collection.Find(filter).First();
        }
        //
        //public void UpdateOne(string searchId, string propertyToUpdate,string value)
        //{
        //    var updateDef = Builders<T>.Update.Set(s.propertyToUpdate] => );
        //}

        public void DeleteOne(string AcctNumber,DeleteItem MyFunc)
        {
            MyFunc(collection,AcctNumber);
        }

       
    }

}
