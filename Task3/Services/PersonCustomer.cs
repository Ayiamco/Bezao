﻿using System;
using System.Collections.Generic;
using System.Text;
using Task3.Helpers;
using Task3.Interfaces;
using MongoDB.Bson.Serialization.Attributes;

namespace Task3.Services
{
    public class PersonCustomer : Customer, ICustomer
    {
        [BsonElement("title")]
        private string title { get; set; }

        [BsonElement("firstName")]
        private string firstName { get; set; }

        [BsonElement("lastName")]
        private string lastName { get; set; }

        [BsonElement("dob")]
        private string dob { get; set; }

        [BsonElement("gender")]
        public Gender gender { get; }

        internal PersonCustomer() { }
        internal PersonCustomer(string title, string firstName, string lastName, string dob, Gender gender, string address)
        {
            this.title = title;
            this.firstName = firstName;
            this.lastName = lastName;
            if (Utility.CheckDOB(dob))
                this.dob = dob;
            else
                throw new ArgumentException("Invalid Date of birth");
            this.gender = gender;
            Address = address;

        }

        public override string Age
        {
            get
            {
                return Convert.ToString(DateTime.Now.Year - Convert.ToDateTime(dob).Year);
            }
        }

        
        public override string Name
        {
            get
            {
                return $"{title} {firstName} {lastName}";
            }
           
        }

        public override string GetCustomerInfo()
        {

            return $"Name: {title} {Name}, Age: {Age}";

        }


    }
    public enum Gender
    {
        Male, Female, NonBinary
    }
}
