﻿ using System;
using System.Collections.Generic;
using System.Text;
using Task3.Helpers;
using Task3.Interfaces;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Task3.Services
{
    
    class Bank
    {
        [BsonId]
        public ObjectId Id { get;set; }

        [BsonElement("Name")]
        public string Name { get; set; }

        [BsonElement("savingsAccountCount")]
        public long savingsAccountCount { get; set; }

        [BsonElement("studentAccountCount")]
        public long studentAccountCount { get; set; }

        [BsonElement("currentAccountCount")]
        public long currentAccountCount { get; set; }

        [BsonElement("bankCurrentBalance")]
        public decimal bankCurrentBalance { get; set; }

        [BsonElement("TransactionHistory")]
        public List<string> TransactionHistory { get; set; }

        [BsonIgnore]
        public DB<DepositAccount> Controller;
        [BsonIgnore]
        public DB<Bank> bankController;
        //public DB<Bank> bankControllerNew;

        public Bank(string name, bool isNew)
        {
            Name = name;
            TransactionHistory=new List<string>{$"\t\tBank Transaction history for: " +
                $"{name} Bank." };
            Console.WriteLine("");
        }
        public Bank(string name)
        {   
            Name = name;
            //Controller to perform CRUD operations on Account details
            Controller = new DB<DepositAccount>("CBN", $"{Name}DepositAccounts");
            //Controller to perform CRUD operations on Banks details
            bankController = new DB<Bank>("CBN", $"BankList");
            //bankControllerNew = new DB<Bank>("Bank", $"{Name}Table");

            //get list of previously created banks
            List<Bank> bankDetails=bankController.ReadAll();
            if (bankDetails.Count > 0)
            {
                foreach (Bank bank in bankDetails)
                {
                    if (bank.Name == Name)
                    // Instance of bank with same name is already in db
                    {
                        //retrieving details of bank
                        bankCurrentBalance = bank.bankCurrentBalance;
                        savingsAccountCount = bank.savingsAccountCount;
                        studentAccountCount = bank.studentAccountCount;
                        currentAccountCount = bank.currentAccountCount;
                        TransactionHistory = bank.TransactionHistory;
                        break;
                    }
                    else
                        bankController.CreateOne(new Bank(Name, true));
                   
                    Console.WriteLine();
                }
            }
            else
                bankController.CreateOne(new Bank(Name, true));
            TransactionHistory = new List<string>{$"\t\tBank Transaction history for: " +
                $"{name} Bank." };
            Console.WriteLine();
        }
                 
        internal bool AddNewAccount(DepositAccount bankAcct, bool isCashier = true)
        {
            //Add new accounts to bank object
            try
            {
                Controller.CreateOne(bankAcct); // add to bank accounts table
                if (bankAcct is DepositAccount depositAcct)
                {
                    //Update bank properties
                    bankCurrentBalance = bankCurrentBalance + bankAcct.Balance;
                    switch ((int)depositAcct.GetAccountType())
                    {
                        case 0:
                            studentAccountCount++;
                            break;
                        case 1000:
                            savingsAccountCount++;
                            break;
                        case 10000:
                            currentAccountCount++;
                            break;
                    }
                    TransactionHistory.Add($"\nTransaction: Account Created," +
                        $" Account Number:{bankAcct.AccountNumber}, startAmount:{bankAcct.Balance}");
                    bankController.DeleteOne(Name, DeleteBankDetails);
                    bankController.CreateOne(this);
                    
                }
                
                if (isCashier)
                    Notification.NewAccount(bankAcct);
                return true;
            }
            catch
            {
                return false;
            }
            
        }
        internal void Summary()
        {
            List<DepositAccount> accounts=Controller.ReadAll();
            foreach(DepositAccount account in accounts)
            {
                Console.WriteLine("#####################################################################################");
                Console.WriteLine(account.GetAccountInfo());
                Console.WriteLine("#####################################################################################");
            }
            Console.WriteLine($"Bank Name: {Name}\n" + $"savingsAccountCount: {savingsAccountCount}, " +
                              $"studentAccountCount:{studentAccountCount}\n" +
                              $"currentAccountCount: {currentAccountCount}, " +
                              $"bankCurrentBalance: {bankCurrentBalance}");
        }

        internal bool AddToAccount(string AcctNumber, decimal amt)
        {
            List<DepositAccount> accounts = Controller.ReadAll();
            var index = accounts.FindIndex(x => x.AccountNumber==AcctNumber);
            if (index>0)
            {
                accounts[index].SendAlert += Notification.Credit; // multicasting Alert Delegate
                bool isSuccessful = accounts[index].Credit(amt);
                if (isSuccessful)
                {
                    //Update Database with result
                    Controller.DeleteOne(accounts[index].AccountNumber,DeleteDepositAccount);
                    Controller.CreateOne(accounts[index]);
                    TransactionHistory.Add( $"\nTransaction:Credit," +
                        $" Account Number:{AcctNumber}, Amount:{amt}");
   
                    bankController.DeleteOne(Name, DeleteBankDetails);
                    bankController.CreateOne(this);
                }
                    
                return isSuccessful;
            }
            else return false;    
        }

        internal bool WithdrawFromAccount(string AcctNumber, decimal amt)
        {
            List<DepositAccount> accounts = Controller.ReadAll();
            Tuple<bool, DepositAccount> result = Utility.FindAccount(accounts, AcctNumber);
            if (result.Item1)
            {
                result.Item2.SendAlert += Notification.Debit; // multicasting Alert Delegate
                bool isSuccessful = result.Item2.Debit(amt);
                if (isSuccessful)
                {
                    //Update Database with result
                    Controller.DeleteOne(result.Item2.AccountNumber, DeleteDepositAccount);
                    Controller.CreateOne(result.Item2);
                    TransactionHistory.Add( $"\nTransaction:Debit," +
                        $" Account Number:{AcctNumber}, Amount:{amt}");
                    bankController.DeleteOne(Name, DeleteBankDetails);
                    bankController.CreateOne(this);
                }

                return isSuccessful;
            }
            else return false;
        }

        internal bool TransferCash(string sender, string _receiver, decimal amt)
        {
            List<DepositAccount> accounts = Controller.ReadAll();
            Tuple<bool, DepositAccount> senderResult = Utility.FindAccount(accounts, sender);
            Tuple<bool, DepositAccount> receiverResult = Utility.FindAccount(accounts, _receiver);
            if(senderResult.Item1==false || receiverResult.Item1 == false)
            {
                throw new ArgumentException("Reciever or Sender Account does not exist");
            }
            bool IsWithdrawSucessful = this.WithdrawFromAccount(senderResult.Item2.AccountNumber, amt);
            bool IsDepositSucessful =  this.AddToAccount(receiverResult.Item2.AccountNumber, amt);
            if (IsDepositSucessful && IsWithdrawSucessful)
                return true;
            else if (IsWithdrawSucessful == false && IsDepositSucessful)
            {//money  did not leave senders account but got to reciever
                bool hasFalseDeposit = true;//reciever account
                do
                {
                    if (this.WithdrawFromAccount(receiverResult.Item2.AccountNumber, amt))
                        hasFalseDeposit = false;
                } while (hasFalseDeposit);
                return false;
            }
            else if (IsDepositSucessful == false && IsWithdrawSucessful)
            {//money left senders accoint but did not get recievers account
                bool hasFalseWithrawal = true;//reciever account
                do
                {
                    if (this.AddToAccount(senderResult.Item2.AccountNumber, amt))
                        hasFalseWithrawal = false;
                } while (hasFalseWithrawal);
                return false;
            }
            else
                return false;
        }
        internal string ViewAccountHistory(string AcctNumber)
        {// Get account transaction history
            List<DepositAccount> accounts = Controller.ReadAll();
            Tuple<bool, DepositAccount> acct = Utility.FindAccount(accounts, AcctNumber);
            return acct.Item2.GetTransactionHistory();
        }
        internal string GetBankTransactionHistory()
        {//bank transaction hstory
            Console.WriteLine(TransactionHistory.Count);
            if (TransactionHistory.Count > 1)
                return string.Join("\n", TransactionHistory);
            else
                return $"No Transaction for {this.Name} bank as at {DateTime.Now}";
        }

        public static void DeleteDepositAccount(IMongoCollection<DepositAccount> collection, string AccountNumber)
        {//DeleteItem Delegate to delete Bankdetails used by DB model to delete a document
            collection.DeleteOne(s => s.AccountNumber == AccountNumber);
        }

        public void DeleteBankDetails(IMongoCollection<Bank> collection, string bankName)
        {//DeleteItem Delegate to delete Bankdetails used by DB model to delete a document
            collection.DeleteOne(s => s.Name == bankName);
        }
    }
}
