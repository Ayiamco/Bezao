﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;
using Task3.Interfaces;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Task3.Services
{
    internal abstract class Account
    {
        [BsonId]
        public string AccountNumber { get;set; }

        [BsonElement("DateOpened")]
        public DateTime DateOpened { get; private set; }

        public string Name { get; } 
        protected void SetAccountNumber()
        {
            Random rand = new Random();
            StringBuilder acctnum = new StringBuilder();
            for (int i = 0; i < 10; i++)
                acctnum.Append(rand.Next(9));
            AccountNumber = Convert.ToString(acctnum);
        }
        protected void SetDateOpened()
        {
            DateOpened = DateTime.Now;
        }

        public abstract bool Credit(decimal amt);
        public abstract string GetAccountInfo();
    }
}
