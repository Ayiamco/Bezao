﻿using System;
using System.Collections.Generic;
using System.Text;
using Task3.Helpers;
using Task3.Interfaces;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Task3.Services
{
    internal delegate bool AlertNotification(
        DepositAccount RecieverAcct,decimal amt ,params DepositAccount[]SenderAcct
        ); // delegate for Alert notifications
    [BsonDiscriminator("DepositAccount")]
    class DepositAccount : Account, IAccount
    {
        //Event
        internal event AlertNotification SendAlert; // Alert event
        //########################################################################################

        //Feilds
        DateTime LastWithdrawalDate;
        decimal balance;
        [BsonElement("AccountHistory")]
        private List<string>AccountHistory = new List<string>();
        //########################################################################################

        //Properties

        
        //public ObjectId Id { get; set; }

        [BsonElement("Customer")]
        public PersonCustomer Customer { get; set; }

        [BsonElement("Balance")]
        public decimal Balance
        {
            get { return balance; }
            set
            {
                if (value < (int)Type)
                {
                    throw new ArgumentException($"Start Balance  of {value} is less than minimum required for "
                        +   Type + " account");
                }
                else
                {
                    balance = value;
                }
            }
        }

        [BsonElement("AccountType")]
        public DepositAccountType Type { get; set; }
        //########################################################################################

        //Constructor ----------------------------------------------------------------------------
        internal DepositAccount() { }
        internal DepositAccount(PersonCustomer customer, DepositAccountType type, decimal startBalance = 0)
        {
            {
                if (Utility.IsValidStartBalance(type, startBalance))
                {
                    Balance = startBalance;
                    Type = type;
                    SetDateOpened();
                    SetAccountNumber();
                    Customer = customer;
                   AccountHistory.Add($"\n\t\tTransaction history for {Customer.Name}");
                }
                else
                {
                    Console.WriteLine($"\n\t\tInvalid StartBalance Error: " +
                        $"{ startBalance} is less than { type} account " +
                        $"minimum balance of {(int)type}");
                    Exception e = new Exception($"Invalid StartBalance");
                    throw e;

                }

            }
        }
        //#########################################################################################

        //EventHandler------------------------------------------------------------------------------

        protected virtual bool OnSendAlert(DepositAccount RecieverAcct, decimal amt, params DepositAccount[] SenderAcct)
        {
            //if "SendAlert" event was not multicasted with any delegate then "SendAlert" remains null
            //and "OnSendAlert" returns false . If the reverse happens the delegate SendAlert takes in 
            //the accounts and amt sends the notification and return the success status(bool)
            return SendAlert==null ? false:SendAlert( RecieverAcct,amt,SenderAcct);
                
        }
        //########################################################################################

        //Non static Methods----------------------------------------------------------------------

        internal DepositAccountType GetAccountType()

        {
            //
            // Summary:
            //     Gets the number of characters in the current System.String object.
            //
            // Returns:
            //     The number of characters in the current string.
            return Type;
        }

        public override string GetAccountInfo()
        {
            string return_string = $" Name: {Customer.Name},  Account Type: {Type},  DateOpened:{DateOpened}\n" +
            $"Balance: {balance}, Account Number: { AccountNumber}";
            return return_string;
        }
        
        internal string GetTransactionHistory()
        {
            return String.Join("\n",AccountHistory);
        }
        public bool Debit(decimal amt)
        {//remove money from account
            if (amt < balance - (decimal)this.GetAccountType())
            {
                balance = balance - amt;
                LastWithdrawalDate = DateTime.Now;
                AccountHistory.Add($"Transaction:Debit, Amount:{amt}");
                OnSendAlert(this, amt);
                return true;
            }
            else
            {
                //Exception e = new Exception($"Insufficient Fund Error: \nBalance: {balance} ," +
                //    $" Withdrawable Balance{balance - (decimal)this.GetAccountType()} ");
                //throw e;
                Console.WriteLine($"\n\t\tInsufficient Fund Error: " +
                    $"Dear {Customer.Name}, {amt} is greater than withdrawable balance of {balance - (decimal)this.GetAccountType()}." +
                    $"###############################################################################################################");
                return false;
            }
        }

        public override bool Credit(decimal amt)
        {//Add money to account
            if (amt > 100000 && Type == DepositAccountType.Savings)
            {// savings account cannot take more than $100,000 at in one instance
                Console.WriteLine("\n\t\tInvalid Account Error:");
                Console.WriteLine($"{Type} account cannot take amount upto {amt} Naira.");
                Console.WriteLine($"Please open a {DepositAccountType.Current} acount" +
                    $"###########################################################################################");
                return false;
            }
            else if (amt > 50000 && Type == DepositAccountType.Student)
            {//Student account cannot take more than $50,000 in one instance
                Console.WriteLine("\n\t\tInvalid Account Error:");
                Console.WriteLine($"{Type} account cannot take amount upto {amt} Naira.");
                if (amt > 100000 && Type == DepositAccountType.Savings)
                {
                    Console.WriteLine($"Please open a {DepositAccountType.Current} acount");
                }
                else
                {
                    Console.WriteLine($"Please open a {DepositAccountType.Savings} or {DepositAccountType.Current} acount");
                }
                return false;
            }
            else
            {// the credit amt is valid for the account type.
                balance = balance + amt;
                AccountHistory.Add($"Transaction:Credit, Amount:{amt}");
                OnSendAlert(this,amt);
                return true;
            }
        }
        //########################################################################################
        //


    }
    public enum DepositAccountType
    {
        Student = 0, Savings = 1000, Current = 10000
    }
}

