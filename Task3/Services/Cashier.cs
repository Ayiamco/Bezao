﻿using System;
using System.Collections.Generic;
using System.Text;
using Task3.Services;
using Task3.Interfaces;

namespace Task3.Helpers
{
    public static class Cashier
    {
       
        private static Tuple<bool, PersonCustomer> GenerateNewCustomer()
        {
            Console.Write("Enter Your title: ");
            string title = Console.ReadLine();
            Console.Write("Enter Your FirstName: ");
            string firstName = Console.ReadLine();
            Console.Write("Enter Your lastName: ");
            string lastName = Console.ReadLine();
            Console.Write("Enter Your date of birth: ");
            PersonCustomer newCustomer = new PersonCustomer();
            try
            {
                string dob = Console.ReadLine();
                Convert.ToDateTime(dob); // check if date is valid
                Console.Write("Enter Your Gender: ");
                try
                {
                    string gender = Console.ReadLine();
                    Console.Write("Enter Your address: ");
                    string address = Console.ReadLine();

                    try
                    {
                        newCustomer = new PersonCustomer(title, firstName, lastName, dob,
                            Utility.getGender(gender), address);
                        return Tuple.Create(true, newCustomer);
                    }
                    catch
                    {
                        Console.WriteLine("\t\tError: Could not create Account, Try again");
                        return Tuple.Create(false, newCustomer);
                    }
                }
                catch
                {
                    Console.WriteLine("\t\tError: Gender is in valid");
                    return Tuple.Create(false, newCustomer);
                }
            }
            catch
            {
                Console.WriteLine("\t\tError: Invalid Date of birth");
                return Tuple.Create(false, newCustomer);
            }
        }


        internal static void CreateAccount(Bank mybank)
        {
            PersonCustomer newCustomer; decimal startAmt;
            Console.Write("Choose your Deposit Account Type\n Reply with Student,Savings or Current:  ");
            try
            {
                DepositAccountType acctType = Utility.GetDepositAccountType(Console.ReadLine());
                Console.Write("Enter Intial Deposit: ");
                try
                {
                    startAmt = Utility.GetDecimal(Console.ReadLine());
                    Tuple<bool, PersonCustomer> newTuple = GenerateNewCustomer();
                    if (newTuple.Item1)
                    {
                        newCustomer = newTuple.Item2;
                        DepositAccount acctDep = new DepositAccount(newCustomer, acctType, startBalance: startAmt);
                        mybank.AddNewAccount(acctDep);
                    }
                }
                catch (ArgumentException)
                {
                    Console.WriteLine($"\t\tError: Invalid Start Balance.");
                    CreateAccount(mybank);
                }
            }
            catch (ArgumentException)
            {// invalid account type 
                Console.WriteLine($"\t\tError: Invalid Account Type");
                CreateAccount(mybank);
            }





        }

        internal static void StartWork(Bank mybank)
        {
            while (true)
            {
                Console.WriteLine($"\n\nWelcome to {mybank.Name}, How can I help you? ");
                Console.WriteLine("Choose your option:");
                Console.WriteLine("1. Create New Dummy Account");
                Console.WriteLine("2. Withdraw from account");
                Console.WriteLine("3. Send money to Another Account");
                Console.WriteLine("4. Deposit money ");
                Console.WriteLine("5. Get Account Info ");
                Console.WriteLine("6. Get Account Transaction History");
                Console.WriteLine("7. Get Bank Transaction History");
                Console.Write("Enter your option: ");
                int option;

                while (true)
                {
                    try
                    {
                        option = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("");
                        if (option > 7 || option < 1)
                        {
                            Console.WriteLine("Error: Enter a valid option");
                            continue;
                        }
                        break;
                    }
                    catch { Console.WriteLine("Error: Enter a valid option"); }

                }
                decimal amt;
                switch (option)
                {
                    case 1:
                        Cashier.CreateAccount(mybank);
                        break;
                    case 2:
                        Console.Write("Enter Account number:");
                        string acctNum = Console.ReadLine();
                        Console.Write("Enter Amount:");
                        amt = Utility.GetDecimal(Console.ReadLine());
                        mybank.WithdrawFromAccount(acctNum, amt);
                        break;
                    case 3:
                        Console.Write("Enter Sender Account Number:");
                        string senderAcctNum = Console.ReadLine();
                        Console.Write("Enter Reciever Account Number:");
                        string receiverAcctNum = Console.ReadLine();
                        Console.Write("Enter Amount:");
                        amt = Utility.GetDecimal(Console.ReadLine());
                        mybank.TransferCash(senderAcctNum, receiverAcctNum, amt);
                        break;
                    case 4:
                        Console.Write("Enter Account number:");
                        string acctNum1 = Console.ReadLine();
                        Console.Write("Enter Amount:");
                        amt = Utility.GetDecimal(Console.ReadLine());
                        mybank.AddToAccount(acctNum1, amt);
                        break;
                    case 5:
                        Console.Write("Enter Account Number: ");
                        mybank.ViewAccountHistory(Console.ReadLine());
                        Console.WriteLine("################################################");
                        break;
                    case 6:
                        Console.Write("Enter Account Number: ");
                        Console.WriteLine(mybank.GetBankTransactionHistory());
                        Console.WriteLine("################################################");
                        break;
                    case 7:
                        Console.WriteLine(mybank.GetBankTransactionHistory());
                        Console.WriteLine("################################################");
                        break;
                }

                string reply;
                while (true)
                {
                    Console.Write("\nDo you want to continue(Y/N): ");
                    reply = Console.ReadLine();
                    if (reply.ToLower() == "y" || reply.ToLower() == "n")
                    {
                        break;
                    }

                    else { Console.WriteLine("Error: please enter Y/N"); }
                }
                Console.WriteLine("");
                if (reply.ToLower() == "n")
                    break;


            }
            mybank.Summary();
        }

    }
}
