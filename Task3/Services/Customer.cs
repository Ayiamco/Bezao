﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task3.Interfaces;

namespace Task3.Services
{
    abstract public class Customer : ICustomer
    {
        public virtual string Age { get; set; }
        public virtual string Name { get; set; }
        public string Address { get; set; }

        public abstract string GetCustomerInfo();
    }
}
