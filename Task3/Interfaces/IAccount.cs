﻿using System;
using Task3.Services;

namespace Task3.Interfaces
{
    interface IAccount
    {
        decimal Balance { get; set; }
        PersonCustomer Customer { get; set; }
        bool Credit(decimal amt);
        string AccountNumber { get; set; }

        string Name { get; }
        string GetAccountInfo();

    }
}