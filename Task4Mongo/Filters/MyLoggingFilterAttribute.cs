﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task4.Models;

namespace Task4.Filters
{
    public class MyLoggingFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var request = filterContext.HttpContext.Request;
            var localIp = filterContext.HttpContext.Connection.LocalIpAddress.ToString();
            
            
            string logString = $"Action Name:{filterContext.ActionDescriptor.DisplayName}\n" +
                $"Status Code: {request.HttpContext.Response.StatusCode}\n" +
                $"Method Type: {request.Method}\n" +
                $"LocalIp: {localIp}\n" +
                $"Time: {DateTime.UtcNow}\n" +
                $"##################################################################\n";
            _Logger.Log(logString);
            base.OnActionExecuted(filterContext);
        }
    }
}
