﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using Task4.Filters;
namespace Task4.Models
{
    [MyLoggingFilter]
    public class UserController : Controller
    {
        
        private Dictionary<string,string> Data { get; set; }
        public IMongoCollection<User> GetCollection()
        {
            var client = new MongoClient("mongodb://127.0.0.1:27017");
            var database = client.GetDatabase("AppUser");
            var collection = database.GetCollection<User>("Users");
            return collection;
        }
        public User GetUser(string userName)
        {
            try
            {
                User user = GetCollection().Find<User>((x) => x.UserName == userName).First<User>();
                return user;
            }
            catch
            {
                return new User();
            }
            
        }
        public bool EditUserDetails(User user, string propertyValue,int propertyIndex )
        {   
            switch (propertyIndex)
            {
                case 1:
                    user.FirstName = propertyValue;
                    break;
                case 2:
                    user.LastName = propertyValue;
                    break;
                case 3:
                    user.Email = propertyValue;
                    break;
                case 4:
                    user.Password = propertyValue;
                    break;
            }
            //  await GetCollection().ReplaceOneAsync(x => x.Id == appointment.Id, appointment);
            try { GetCollection().ReplaceOne(x => x.UserName == user.UserName, user); return true; }
            catch { return false; }  
        }

        [MyLoggingFilter]
        public ActionResult Index(Dictionary<string,string> Data)
        {
            TempData["Name"] = Data["Name"];
            return View();
        }
        public ActionResult Register()
        {
            return View("Register");
        }
        public ActionResult LogOut()
        {
            return RedirectToAction("Login");
        }
        public ActionResult Login()
        {
            return View("Login");
        }
        public ActionResult Edit()
        {   
            return View();
        }
        public ActionResult Delete()
        {
            return View();
        }

        //[HandleError()]
        [HttpPost]
        public ActionResult Edit(string property, string propertyValue, string userName,string password)
        {
            Dictionary<string, int> EditableProperties = new Dictionary<string, int>()
            {
                {"FirstName",1 },
                {"LastName",2 },
                {"Email",3 },
                {"Password",4},
            };
            User user = GetUser(userName);
            if(user.Password==password && password != null)
            {
                bool resp=   EditUserDetails(user, propertyValue, EditableProperties[property]);
                if (resp == true)
                    return Content("User Details Changed Suceessfully!!!");
                else
                    return Content("User Details Change was not Suceessful!!!");
            }

            return Content("UserName or Password was not correct");
        }

        [HttpPost]
        public ActionResult Register(string FirstName, string LastName,string UserName, string Email,string pass1, string pass2)
        {
            if (pass1 == pass2)
            {
                
                try
                {
                    User user = GetCollection().Find<User>(
                    (x) => x.UserName == UserName).First<User>();
                    return Content("Username Already Exist");
                }
                catch
                {
                    User newUser = new User()
                    {
                        FirstName = FirstName,LastName = LastName,
                        UserName = UserName, Email = Email,
                        Password = Convert.ToString(pass1),
                    };
                    GetCollection().InsertOne(newUser);

                    Data = new Dictionary<string, string>();
                    Data.Add("Name", newUser.FirstName + " " + newUser.LastName);
                    return RedirectToAction("Index", Data);
                }     
            }
            else
                return Content($"Password Did match {pass1},{pass2}");
                
        }

        [HttpPost]
        public ActionResult Login(string userName, string password)
        {
            //
            //code to check if password matches
            //
            User user = GetUser(userName);
            if (user.Password == password && password!=null)
            {
                //password and username has match
                return RedirectToAction("Index",new Dictionary<string, string>()
                {
                    { "Name",user.FirstName+" "+user.LastName }
                });
            }
            else
            {
                //password or username is wrong
                return Content("Password or username is not correct");
            }
            
        }

        [HttpPost]
        public ActionResult Delete(string userName, string password)
        {
            User user = GetUser(userName);
            if (user.Password == password && password != null)
            {
                GetCollection().DeleteOne(x => x.UserName == userName);
                return Content($"Successfully Deleted {userName} account");
            }
            else
                return Content($"Failed to delete {userName} account");
        }
        


    }
}
