﻿let select = document.getElementById("property")
select.addEventListener("change", (e) => {
    let options = ["FirstName", "LastName", "Email", "Password"]
    let value = document.getElementById("property").selectedIndex
    
    if (options[value] == "Email") {
        document.getElementById("propertyValue").setAttribute("type", "email")
        document.getElementById("password-hidden").style.display = "none"
    }
    else if (options[value] == "Password") {
        document.getElementById("password-hidden").style.display = "block"
        document.getElementById("propertyValue").setAttribute("type", "text")
    }
    else {
        document.getElementById("propertyValue").setAttribute("type", "text")
        document.getElementById("password-hidden").style.display = "none"
    }
})
