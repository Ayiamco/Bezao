﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Task4MySQL.Filters;
using Task4MySQL.Services;
using Task4MySQL.Models;

namespace Task4MySQL.Controllers
{
    //[MyLoggingFilter]
    
    public class UserController : Controller
    {
        public void EditUserDetails(User user, string propertyValue, int propertyIndex)
        {
            switch (propertyIndex)
            {
                case 1:
                    user.FirstName = propertyValue;
                    break;
                case 2:
                    user.LastName = propertyValue;
                    break;
                case 3:
                    user.Email = propertyValue;
                    break;
                case 4:
                    user.Password = propertyValue;
                    break;
            }

        }

        [MyLoggingFilter]
        public ActionResult Index(Dictionary<string, string> Data)
        {
            try {
                TempData["Name"] = Data["Name"];
                return View();
            }
            catch 
            {
                return View("Login");
            }
           
        }
        public ActionResult Register()
        {
            return View("Register");
        }
        public ActionResult LogOut()
        {
            return RedirectToAction("Login");
        }
        public ActionResult Login()
        {
            return View("Login");
        }
        public ActionResult Edit(string Username)
        {
            TempData["Username"] = Username;
            return View();
        }
        public ActionResult Delete()
        {
            return View();
        }


        //-----------------------Post Requests-------------------------------------
        [HttpPost]
        public ActionResult Edit(string property, string propertyValue, string Username,string propertyValueOptional=null )
        {
            DataBaseWorker dbController = new DataBaseWorker();
            if (property != "Password")
            {
                dbController.Edit(property, propertyValue, Username);
                return Content($"{property} Changed to {propertyValue}");
            }
            else
            {
                if ( String.IsNullOrEmpty(propertyValueOptional) || propertyValueOptional != propertyValue)
                {
                    throw new Exception("Password did not match or is empty");
                }
                else
                {
                    dbController.Edit(property, propertyValue, Username);
                    return Content($"{property} Changed to {propertyValue}");
                }
            }
            
        }

        [HttpPost]
        public ActionResult Register(string FirstName, string LastName, string UserName, string Email, string pass1, string pass2)
        {
            if (pass1 == pass2)
            {
                DataBaseWorker dbController = new DataBaseWorker();
                try
                {
                    dbController.Register(FirstName, LastName, UserName, Email, pass2);
                }
                catch (System.Data.SqlClient.SqlException)
                {
                    User user = new User()
                    {
                        UserName = UserName,
                        Email = Email,
                        LastName = LastName,
                        FirstName = FirstName
                    };
                    ViewBag.Error = true;
                    return View(user);
                }

                return Content("Successful");
            }
            else
                return Content($"Password Did match {pass1},{pass2}");

        }

        [HttpPost]
        public ActionResult Login(string userName, string password)
        {
            DataBaseWorker dbController = new DataBaseWorker();
            try
            {
                User user = dbController.Login(userName, password);
                TempData["Name"] = user.FirstName + " " + user.LastName;
                TempData["Username"] = userName;
                
            }
            catch (Exception ex)
            {
                if (ex.Message == "Username Does not Exist" || ex.Message == "Incorrect Password")
                {
                    ViewBag.Username = ex.Message == "Incorrect Password" ? userName : "";
                    ViewBag.Message = ex.Message;
                    return View();
                }
                else
                {
                    ViewBag.Message = "Unknown Error Occurred.";
                    return View();
                }
            }
            return View("Index");



        }

        //        [HttpPost]
        //        public ActionResult Delete(string userName, string password)
        //        {
        //            User user = GetUser(userName);
        //            if (user.Password == password && password != null)
        //            {
        //                //Delete account from database
        //                return Content($"Successfully Deleted {userName} account");
        //            }
        //            else
        //                return Content($"Failed to delete {userName} account");
        //        }



    }
}

