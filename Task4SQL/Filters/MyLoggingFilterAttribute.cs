﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Task4MySQL.Models;

namespace Task4MySQL.Filters
{
    public class MyLoggingFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var request = filterContext.HttpContext.Request;
            //request.IsAuthenticated;
            string logString = $"Action Name:{filterContext.ActionDescriptor.ActionName}\n" +
                $"Status Code: {filterContext.HttpContext.Response.StatusCode}\n" +
                $"LocalIp: {request.UserHostAddress}\n" +
                $"Time: {DateTime.UtcNow}\n" +
                $"##################################################################\n";
            MyLogger.Log(logString);
            base.OnActionExecuted(filterContext);
        }
    }
}
