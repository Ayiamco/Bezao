﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Task4MySQL.Models;

namespace Task4MySQL.Services
{
    public class DataBaseWorker
    {
        readonly DataBaseConnection conn; 
        public DataBaseWorker()
        {
            conn = new DataBaseConnection();
        }

        public void Register(string username, string password,string firstname, string lastname, string email)
        {
            conn.OpenConnection();
            string query = $"INSERT INTO Users (FirstName,LastName,UserName,Email,Password) VALUES('{firstname}', '{lastname}', '{username}', '{email}', '{password}')";
            
            using (SqlCommand cmd = new SqlCommand(query, conn.sqlCon))
            {
              
                int count=cmd.ExecuteNonQuery();
            }
            conn.CloseConnection();
        }

        public User Login (string username, string password)
        {
            User user = new User();
            conn.OpenConnection();
            string query = $"SELECT * FROM Users Where UserName='{username}'";
            using (SqlCommand cmd = new SqlCommand(query, conn.sqlCon))
            {
                using (SqlDataReader data = cmd.ExecuteReader())
                {
                    int count = 0;
                    while (data.Read())
                    {
                        user.FirstName = data["FirstName"].ToString();
                        user.LastName = data["LastName"].ToString();
                        user.Email = data["Email"].ToString();
                        user.UserName = username;
                        user.Password = data["Password"].ToString();
                        if (user.Password != null)
                            count++;
                        break;
                        
                    }
                    conn.CloseConnection();
                    if (count == 0) { throw new Exception("Username Does not Exist"); }
                }
                
                if (user.Password != password)
                    throw new Exception("Incorrect Password");
            }
            return user;


        }

        public void Edit(string property, string value,string username)
        {
            conn.OpenConnection();
            string query = $"UPDATE Users SET {property} = '{value}'    WHERE UserName='{username}'";
            using (SqlCommand cmd = new SqlCommand(query, conn.sqlCon))
            {
                int count = cmd.ExecuteNonQuery();
            }
            conn.CloseConnection();
        }
    }
}