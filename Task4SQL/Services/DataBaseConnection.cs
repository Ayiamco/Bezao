﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Task4MySQL.Services
{
    public class DataBaseConnection
    {
        public SqlConnection sqlCon;

        public DataBaseConnection()
        {
            sqlCon = new SqlConnection(@"Data Source=DESKTOP-QVCBN0R\JOSEPH;Initial Catalog=Task4Users;Integrated Security=True");
        }
        public void OpenConnection()
        {
            if (sqlCon.State != ConnectionState.Open)
            {
                sqlCon.Open();
            }
        }
        public void CloseConnection()
        {
            if (sqlCon.State != ConnectionState.Closed)
            {
                sqlCon.Close();
            }
        }
    }
}
