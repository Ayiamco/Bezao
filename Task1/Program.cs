﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace BezoaCSharpTask
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task1.GetAge(); 
            //object[] source = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            Console.Write("Enter the start index: ");
            string start = Console.ReadLine();
            Console.Write("Enter the stop[ insex: ");
            string stop = Console.ReadLine();
            object[] myarray = SliceArray(new object[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 }, Convert.ToInt32(start), Convert.ToInt32(stop));
            Console.WriteLine(String.Join(',', myarray));



        }


        static T[] SliceArray<T>(T[] sourceArray, int start, int stop)
        {
            int length = stop - start + 1;
            T[] destinationArray = new T[length];
            Array.Copy(sourceArray, start, destinationArray, 0, length);
            return destinationArray;
        }
    }
    
}
